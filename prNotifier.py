import os
import schedule
import time
import logging
from slack.web.client import WebClient
from slack.errors import SlackApiError
from jira import JIRA
import requests
from requests.auth import HTTPBasicAuth


logging.basicConfig(level=logging.DEBUG)


def main():
  # Initialize Slack Client
  SLACK_BOT_TOKEN = os.environ['SLACK_BOT_TOKEN']
  slack_client = WebClient(SLACK_BOT_TOKEN)
  logging.debug("authorized slack client")

  # Initialize Jira Client
  JIRA_TOKEN = os.environ['JIRA_TOKEN']
  JIRA_USERNAME = os.environ['JIRA_USERNAME']
  jira_client = JIRA(options={"server": 'https://testingthingsout.atlassian.net'}, basic_auth=(str(JIRA_USERNAME), str(JIRA_TOKEN)))

  # Intialize client call to Jira interal API
  auth=HTTPBasicAuth(JIRA_USERNAME, JIRA_TOKEN)


  issue_ids = get_issue_ids(jira_client)
  msg = get_and_format_pr_info(issue_ids, auth)

  # Schedule message
  schedule.every(5).seconds.do(lambda: send_message(slack_client, msg))   # schedule.every().monday.at("13:15").do(lambda: sendMessage(slack_client, msg))

  logging.info("Starting polling while loop")
  while True:
    schedule.run_pending()
    time.sleep(5) # sleep for 5 seconds between checks on the scheduler


def get_issue_ids(jira_client):
  """
  Pass in initialized Jira client
  Returns list of issue/ticket IDs that have OPEN prs
  """
  # Get all issues with OPEN PRs
  open_pr_jql = 'development[pullrequests].open = 1 order by created desc'
  issues_in_proj = jira_client.search_issues(open_pr_jql)

  open_pr_issue_ids = [issue.id for issue in issues_in_proj]

  return open_pr_issue_ids


def get_and_format_pr_info(open_pr_issue_ids, auth):
  msg='*Currently Open PRs*\n'
  headers={'Content-Type' :'application/json'}

  for issue_id in open_pr_issue_ids:
    print(f"Getting info for issue id {issue_id}")

    # Get info about open PR
    URL = f"https://testingthingsout.atlassian.net/rest/dev-status/1.0/issue/detail?issueId={issue_id}&applicationType=bitbucket&dataType=pullrequest"
    r = requests.get(url = URL, auth=auth, headers=headers)

    data = r.json()

    # Format info and add to final message
    pull_requests = data["detail"][0]["pullRequests"]
    for pr in pull_requests:
      pr_author = pr["author"]["name"]
      pr_name = pr["name"]
      pr_last_update = pr["lastUpdate"]
      pr_link = pr["url"]
      msg += f"- <{pr_link}|{pr_name}> by {pr_author} last updated {pr_last_update}\n"

    return msg


def send_message(slack_client, msg):
  # make the POST request through the python slack client
  try:
    slack_client.chat_postMessage(
      channel='#test-pr-notifier', # TODO: need to make this an env var or something
      text=msg
    )
  except SlackApiError as e:
    logging.error('Request to Slack API Failed: {}.'.format(e.response.status_code))
    logging.error(e.response)


if __name__ == "__main__":
  main()